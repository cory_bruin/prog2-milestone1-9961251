﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App3
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}
        
        private void CalcButton_Clicked(object sender, EventArgs e)
        {
            var input = InputBox.Text;
            var convert = Convert.ToInt32(input);
            var test = Calc.calc(convert);
            Result.Text = $"Result:";
            Result1.Text = $"{test}";
        }
    }
}
