﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App3
{
    class Calc
    {
        public static int calc(int c)
        {
            var result = 0;
            int i = 0;
            while (i < c)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    result += i;
                }
                i++;
            }
            return result;
        }
    }
}
