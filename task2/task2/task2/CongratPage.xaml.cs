﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace task2
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CongratPage : ContentPage
	{
		public CongratPage (int guess, int guesses)
		{

			InitializeComponent ();
            Number.Text = $"You guessed the correct number: {guess}";
            Guesses.Text = $"Guesses: {guesses}";
		}
        async void HomeButton_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
            Navigation.RemovePage(this);
        }
    }
}
