﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace task2
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GamePage : ContentPage
	{
        int rand;
        int guesses = 0;
        public GamePage (int sValue)
		{
			InitializeComponent ();
            DiffLevel.Text = $"A random number has been selected between 1 and {sValue}";
            Random r = new Random();
            rand = r.Next(1, sValue);
        }
        public void GuessButton_Clicked(object sender, EventArgs e)
        {
            var guess = GuessBox.Text;
            var convert = Convert.ToInt32(guess);
            var answer = Calc.calc(rand, convert);
            if (answer == 0)
            {
                guesses = guesses + 1;
                Guesses.Text = $"Guesses: {guesses}";
                Navigation.PushAsync(new CongratPage(convert, guesses));
                Navigation.RemovePage(this);
            }
            else if (answer > 0)
            {
                HighLow.Text = "Higher";
                guesses = guesses + 1;
                Guesses.Text = $"Guesses: {guesses}";
                guessBGC.BackgroundColor = Color.Red;
            }
            else { HighLow.Text = "Lower"; guesses = guesses + 1; Guesses.Text = $"Guesses: {guesses}"; guessBGC.BackgroundColor = Color.Red; }
        }
    }
}
