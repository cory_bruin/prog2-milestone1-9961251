﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace task2
{
	public partial class MainPage : ContentPage
	{
        int sValue;
		public MainPage()
		{
			InitializeComponent();
            sValue = 500;
        }
        public void Slider_Change(object sender, ValueChangedEventArgs e)
        {
            sValue = (int)Math.Round(e.NewValue);
            SliderValue.Text = $"Difficulty: Random number between 1 and {sValue}";
        }
        async void StartButton_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new GamePage(sValue));
        }
    }
}
